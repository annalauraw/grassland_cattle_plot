This repository contains a Jupyter Notebook which explains the underlying data processing of the visualization "Grassland vs. cattle per municipality", available at [ecoviz.ch](https://ecoviz.ch/visualizations/2).

### Data sources

- Federal Office of Topography swisstopo (2022): swissBOUNDARIES3D. Available at [https://www.swisstopo.admin.ch/en/geodata/landscape/boundaries3d.html](https://www.swisstopo.admin.ch/en/geodata/landscape/boundaries3d.html). \[Downloaded May 11th 2022\]
- Federal Office for Agriculture FOAG (1977): Klimaeignungskarte für die Landwirtschaft in der Schweiz. Available at [https://www.blw.admin.ch/blw/de/home/politik/datenmanagement/geografisches-informationssystem-gis/klimaeignungskarte.html#35_1666679206333__content_blw_de_home_politik_datenmanagement_geografisches-informationssystem-gis_klimaeignungskarte_jcr_content_par_tabs](https://www.blw.admin.ch/blw/de/home/politik/datenmanagement/geografisches-informationssystem-gis/klimaeignungskarte.html#35_1666679206333__content_blw_de_home_politik_datenmanagement_geografisches-informationssystem-gis_klimaeignungskarte_jcr_content_par_tabs). \[Downloaded May 8th 2022\]
- Identitas AG (2023): Cattle - Distribution by municipality. Available at [https://opendata.swiss/en/dataset/rinder-verteilung-pro-gemeinde](https://opendata.swiss/en/dataset/rinder-verteilung-pro-gemeinde). \[Validity: 2023-05-31. Evaluated: 2023-06-26\]

### Reproduce the data processing

If you would like to reproduce the data processing or tweak it, you can:

- clone this repository
- create a virtual environment with Python 3.8, e.g. ```conda create --name map_viz python=3.8```
- install the dependencies from requirements.txt, e.g. ```conda install --file requirements.txt```